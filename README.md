Collectif d’Hébergeurs Alternatifs Transparents, Ouverts, Neutres et Solidaires (C.H.A.T.O.N.S.)
------------------------------------------------------------------------

![logo CHATONS](https://chatons.org/sites/all/themes/chatons/css/chatons.png)

CHATONS est le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires.

Il vise à rassembler des structures souhaitant éviter la collecte et la centralisation des données personnelles au sein de silos numériques du type de ceux des GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

CHATONS est un collectif initié par l'association Framasoft suite au succès de sa campagne « [Dégooglisons Internet](https://degooglisons-internet.org) » .

Le projet vise à rassembler des acteurs proposant des services en ligne libres, éthiques, décentralisés et solidaires afin de permettre aux utilisateurs de trouver - rapidement - des alternatives aux produits de Google (entre autres) mais respectueux de leurs données et de leur vie privée.

Pour commencer, vous pouvez :

  * [Découvrir ses premiers membres](https://chatons.org/find)
  * En apprendre plus sur le projet en lisant la [Foire Aux Questions](https://chatons.org/faq)
  * Lire les textes fondateurs du collectif : son [Manifeste](https://framagit.org/framasoft/CHATONS/blob/master/docs/Manifesto-fr.md) et sa [Charte}(https://framagit.org/framasoft/CHATONS/blob/master/docs/Charter-fr.md)
  * Connaître [les ambitions du collectif](https://framablog.org/2016/02/09/chatons-le-collectif-anti-gafam/)
  * Découvrir comment [rejoindre le collectif](https://chatons.org/rejoindre-le-collectif)
