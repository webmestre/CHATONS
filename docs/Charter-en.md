# Charter of KITTENS

## What KITTENS are

KITTENS are Keen Internet Talented Teams Engaged in Network Service. Each member of the Teams, designed below by the name "KITTEN", commits herself to obey the present charter.

Everything which is not explicitly forbidden by the present charter or by law, is authorized.

>  **Associated technical criteria:**
>  - the KITTEN can have various juridic status: moral or physic person (an individual, a company, an association, a cooperative society, etc.).
> 
> **Ethical and social associated criteria:**
> - the KITTEN commits herself to behave respectfully and kindly regarding other members of KITTENS;
> - the KITTEN commits herself to behave respectfully and kindly regarding users of here services.

## Network Service

A Network Service supplier is defined here as an entity hosting data provided by third party people, and providing services where those data are stored, or which those data make their way through.
Users named "hosted" below are the physic or moral people, who can create or modify data based on the infrastructure of the KITTEN.

>  **Technical associated criteria:**
> - the KITTEN must have control on the associated software and data (*root* access);
> - the KITTEN commits herself to publicly and unambiguously show her level of control of the hardware which hosts the services and the associated data. Incidentally, the name of the provider of physical hosting of servers must be notified clearly to the hosted.
> 
> **Ethical and social associated criteria:**
> - The KITTEN commits herself, for all services touching directly or indirectly the hosted, not to use services of third party providers when their practices or conditions of service are not compatible with the principles and conditions of the present charter.

## Keen and Alternative

When proposing free/libre inline services, the KITTEN proposes to be an alternative to the model of companies who provide closed and centralized services which result in a monopoly and perverse usage of personal data.

Such alternatives illustrate the diversity of available free/libre solutions which can be offered for personal or collective usage.

>  **Technical associated criteria:**
> - the KITTEN commits herself to publish documentation about her infrastructure and here services;
> - the KITTEN commits herself not use means to track how the hosted are using services, except for statistic or administrative goals.
> 
> **Ethical and social associated criteria:**
> - the KITTEN commits herself to favor the fundamental freedoms of her users, in particular to respect their private life, in every of her acts;
> - the KITTEN will in no case use services of advertising media. Sponsoring or patronizing, by displaying the name or the identity of partner organizations (name, logo, etc.) is allowed, as long as no personal information is communicated to those partners;
> - the KITTEN must never operate commercially data or meta-data form the hosted.


## Talented and Transparent

The KITTEN ensures the hosted that no unfair usage will ever be done with their data, identities or rights, in particular by publishing concise, easy and straightforward rules for using the services

Users will benefit from available services with a full knowledge of their features, advantages and limits of usage.

No data produced by the means of services is owned by the KITTEN. She adds no legal right, and makes no censorship, as long as the contents obey her General Use Conditions.

>  **Technical associated criteria:**
> - the KITTEN commits herself to be transparent about her technical infrastructure (distribution and software in use). For security reasons, some information (like the version number of a software for example) may not be published;
> - the KITTEN commits herself to do her best (obligation of means) about the security of her infrastructure;
> - the KITTEN commits herself to implement the backup policy which she considers as most appropriated;
> - the KITTEN commits herself to publish anonymous usage statistics;
> - the KITTEN commits herself to publish a log of the technical failures of services.
> 
> **Ethical and social associated criteria:**
> - the KITTEN commits herself to never assume ownership on contents, data or meta-data produced by the hosted or the users;
> - the KITTEN commits herself to publish clearly her service offer, and the associated tariff;
> - the KITTEN commits herself to publish General Use Conditions (GUC) which are prominent, clear, non-ambiguous, and easy to understand by the greatest number;
> - the KITTEN commits herself to publish, inside the GUC, a clause about "respect of personal data and private life" which defines precisely the KITTEN's policy regarding the scheduled usages;
> - the KITTEN commits herself to publish her accounts and activity reports, at less for the part touching her activity as KITTEN;
> - the KITTEN commits herself to let know to the hosted the main information regarding her policy about security and backup (of course, without disclosing information which may damage the aforementioned security);
> - The KITTEN commits herself to communicate with the hosted about difficulties she meets in running her structure and the various services she provides, in particular by providing support and bug report services.

## Engaged and Open

The usage of free/libre software and of open standards for Internet is the only mean for the members to provide their services.

Accessing the source code is fundamental as a principle of Free Software. For every proposed service, the member commits herself to use software and code chunks under free/libre licenses only.

In case of improvement of the code of used software, the member commits herself to publish her improvements under a free license (compatible with the original license), and will encourage every volunteer contribution from users, by inviting them to contact authors.

The member commits herself to keep available the source code either by publishing a link to the official website of the application, or, if the former is no longer available, by publishing the used source code.

>  **Technical associated criteria:**
> - the KITTEN commits herself to use only free/libre distributions ( GNU/Linux, FreeBSD, etc.) as an operating system for the infrastructure of the hosted;
> - the KITTEN commits herself to use only software ruled by free/libre licenses, in the sense of the Free Software Foundation, either compatible or not with the GPL license (the list is available here : http://www.gnu.org/licenses/license-list.html);
> - the KITTEN commits herself to publish annually the list of packages installed on the servers who host services provided to users, for example by using the tool "vrms" under Debian and derivatives, or an equivalent tool for other distributions;
> - the KITTEN commits herself to use open formats only;
> - the KITTEN commits herself, in case of modification of the source code of software used in the services, to publish her modifications (unless when it can damage the security or the private life of users).
> 
> **Ethical and social associated criteria:**
> - the KITTEN commits herself to contribute, technically or financially and within her possibilities, to the movement of free/libre software. Her contributions are listed publicly;
> - the KITTEN commits herself to make easy for the hosted to be able to quit her services, with their associated data in open formats;
> - the KITTEN commits herself to delete definitely any information (account and personal data) regarding the hosted upon her request.


## Network Neutral

Ethics of free/libre software are made of sharing and independence. The member of KITTENS commits herself to use no *a priori* censorship of contents, no tracking of users' actions, and to give no reply to any administrative or authoritative request unless it is a legal request in due form. As a feedback the users are committed, for the production or hosting of their contents, to obey the legal rules of the concerned country.

Equality of access to those applications is a strong commitment: may them be paying, premium of for free, the offers of the members must be done without technical or social discrimination, and provide all warranties of neutrality regarding the flow of data.

Respecting the private life of users is an imperious demand. No personal data will ever be used for commercial means, forwarded to third parties or used for goals not described by the present charter, except for statistic needs and always within the legal rules.

>  **Technical associated criteria:**
> - The KITTEN commits herself to never track the users' actions, unless for administrative, technical means or to improve her internal services;
> - the KITTEN commits herself to make no *a priori* censorship regarding the contents of the hosted;
> - the KITTEN commits herself to protect at best the data of her users from external attacks, in particular by using as much as possible strong cryptography for her data, during their reception/transmission on the network (SSL/TLS) or their storage (files ans databases in particular).
> - the KITTEN commits herself to maintain a list, for the seek of the hosted, of administrative or authoritative requests which she received. In case of a legal impossibility to communicate her information, the KITTEN is encouraged to implement in advance a workaround like a *Warrant Canary*;
> 
> **Ethical and social associated criteria:**
> - the KITTEN commits herself not to reply an administrative or authoritative request which requires access to personal information, before a legal request in due form is presented;
> - the KITTEN commits herself to apply her CGU kindly, if some moderation action becomes necessary.


## Service and Solidarity

In an intent of sharing and helping, every member sends to other members and to the public as much as possible of knowledge to foster the usage of free/libre software, and install free/libre inline services. That sharing of technical and cognitive resources, makes Internet a common good, available for everybody and owned by nobody.

>  **Technical associated criteria:**
> - the KITTEN commits herself to organize herself to be able to meet physically, share with the hosted and make them participate;
> - the KITTEN commits herself to have an active and voluntary behavior regarding access for anybody to proposed services, in particular by obeying standards for [web accessibility](https://en.wikipedia.org/wiki/Web_accessibility).
> 
>  **Ethical and social associated criteria:**
> - the KITTEN commits herself to implement and promote an organizational system which is inclusive, able to accept differences and promote diversity, by taking care that marginal or excluded groups be involved in the development process;
> - the KITTEN commits herself not to exclude *a priori* potential subscribers of the featured services. The Kitten can define a "target public" which she wants to address (for example by geographic criteria, or topics), however she must, as far as possible, ask to non-pertinent queries, for instance by forwarding them to another KITTEN or by making easy to create structures replying to the non-satisfied needs;
> - the KITTEN commits herself to define an economic model based on solidarity. When services are paying, prices must be reasonable and adequate with the means which are implemented. Additionally, the lowest salary - as an equivalent for a full-time job, comprising primes and dividends - cannot be lower than the fourth of the highest salary - as an equivalent for a full-time job, comprising primes and dividends - of the structure;
> - the KITTEN commits herself to share as widely as possible, publicly and under a free/libre license, her knowledge;
> - the KITTEN commits herself to facilitate the participation of the hosted to the maintenance of the services. A particular attention will be brought to "non-technicians" in order to allow them to improve their knowledge and their efficiency and to allow them to play fully their role of contributors;
> - the KITTEN commits herself to facilitate the emancipation of the hosted and of the targeted public, in particular by popular education campaigns (meetings, internal or external trainings, coding feasts, sessions of writing for collaborative documentation, hackathon, etc.) in order to insure that Internet will remain a technology reachable by everybody, for everybody.
